
window.Vue = require("vue");

Vue.component(
    'navigationbar',
    require("./components/NavigationBar.vue").default
);

Vue.component(
    'chatwindow',
    require("./components/ChatWindow.vue").default
);

Vue.component(
    'chatbar',
    require("./components/ChatBar.vue").default
);

Vue.component(
    'membersbar',
    require("./components/MembersBar.vue").default
);

Vue.component(
    'emojipicker',
    require("./components/EmojiPicker.vue").default
);

Vue.component(
    'login',
    require("./components/LoginScreen.vue").default
);

Vue.component(
    'guildcreationscreen',
    require("./components/GuildCreationScreen.vue").default
);

Vue.component(
    'guildsettings',
    require("./components/GuildSettings.vue").default
);

Vue.component(
    'usersettings',
    require("./components/UserSettings.vue").default
);

Vue.component(
    'channelcreationscreen',
    require("./components/ChannelCreationScreen.vue").default
);

Vue.component(
    'smalluserdisplay',
    require("./components/SmallUserDisplay.vue").default
);

Vue.component(
    'cssselectorcheatsheet',
    require("./components/CSSSelectorCheatSheet.vue").default
);

Vue.component(
    'joinguild',
    require("./components/JoinGuild.vue").default
);

Vue.use(Vuex);

const store = require("./app/store/store");

//Helper functions
require('./app/HelperResources');

const AppManager = require('./app/AppManager');

$(document).ready(function () {
    window.store = store;

    window.appManager = new AppManager(store);
    window.appManager.Start(store, function() {
        const app = new Vue({
            el: '#app',
            store,
            computed: {
                loggedIn() {
                    if (window.store.state.user.loggedIn) {
                        return window.store.state.user.loggedIn;
                    } else {
                        return false;
                    }
                }
            }
        });
    });
});

window.events = new Vue();