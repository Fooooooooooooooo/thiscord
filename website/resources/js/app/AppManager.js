const user_manager = require('./UserManager');
const guild_manager = require('./GuildManager');
const channel_manager = require('./ChannelManager');
const message_manager = require('./MessageManager');
const websocket_manager = require('./WebsocketManager');

UserManager = new user_manager();
GuildManager = new guild_manager();
ChannelManager = new channel_manager();
MessageManager = new message_manager();
WebsocketManager = new websocket_manager();

class AppManager {

    Start(store, cb) {
        this.store = store;
        var app = this;

        UserManager.store = store;
        GuildManager.store = store;
        ChannelManager.store = store;

        console.log("Connecting to API service...");
        UserManager.CheckAuthentication(store ,function(loggedIn, user) {
            
            if (!loggedIn) {

                //Check if cookies are disabled
                if (!navigator.cookieEnabled) {
                    alert("Cookies are disabled, please enable them to use Thiscord");
                    return;
                }
    
                cb();
                return;
            }

            console.log("Logging in as " + user.name);
            WebsocketManager.SetupListeners();
            window.store.commit('login', user);
            
            //Load list of guilds
            GuildManager.LoadGuilds(this.store, function() {
                var guild = Object.keys(this.store.state.guilds)[0];

                //If user is in no guilds return
                if (!guild) {
                    window.store.commit('setLoggedIn', true);
                    if (cb) {cb();}
                    return;
                }

                console.log("Loading guild " + guild);
                //Load guild channels
                this.LoadGuild(guild, store, function() {
                    MessageManager.SetupListeners();

                    window.store.commit('setLoggedIn', true);
                    if (cb) {cb();}

                    $("*[channel]").click(function(click) {
                        this.LoadChannel(window.store.state.selectedGuild.guild._id, click.currentTarget.getAttribute('channel'), window.store)
                    }.bind(this));

                    $("*[guild]").click(function(click) {
                        this.LoadGuild(click.currentTarget.getAttribute('guild'), window.store)
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }.bind(this));
    }

    LoadGuild(guild, store, cb) {
        console.log("Loading guild " + guild);
        GuildManager.SelectGuild(guild, function() {
            
            //Get all users in guild
            UserManager.GetUsers(guild, function() {
                //Channel to load
                const channel = Object.entries(store.state.selectedGuild.guild.channels)[0][1]._id;

                this.LoadChannel(guild, channel, store, cb);
     
            }.bind(this));
            
        }.bind(this));
    }

    LoadChannel(guild, channel, store, cb) {
        console.log("Loading channel " + channel);
        store.commit('setSelectedChannel', {guildId: guild, channelId: channel});

        window.socket.emit('join', {
            channel: store.state.selectedGuild.channel._id
        });

        //Fetch all messages in selected channel
        MessageManager.GetMessages(store, guild, channel,  function() {
            if (cb) {cb();}
        });

        
    }
}

module.exports = AppManager;