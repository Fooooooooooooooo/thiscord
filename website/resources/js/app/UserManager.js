const settings = window.settings;

class UserManager {

    StartListeners() {

        //When we leave page send a request saying that we have gone offline
        $(window).on('unload', function() {
            var fd = new FormData();
            fd.append('_token', $('[name="csrf-token"]')[0].content);
            navigator.sendBeacon('/api/user/presence?p=offline', fd);
        });
    }

    CheckAuthentication(store, cb) {

        this.store = store;

        var token = $.cookie('token');

        if (token == null) {
            return cb(false);
        }

        window.settings.token = token;

        $.GET("user/current", function(result, data) {
            if (result == "error") {
                cb(false, data.responseJSON);
            } else {
                cb(true, data.responseJSON);
            }
        });
    }
    GetUsers(guild, cb) {
        $.GET('guild/' + guild + "/users", function(status, data) {

            const currentUId = this.store.state.user._id;
            
            Object.entries(data.responseJSON).forEach(user => {
                if (user[1]._id == currentUId) {
                    data.responseJSON[user[1]._id].presence = 2;
                }
            });

            this.store.commit('addUsers', data.responseJSON);
            cb();
        });
    }
}

module.exports = UserManager;