const CSSParser = require('./CSSParser');

class GuildManager {

    LoadGuilds(store, cb) {

        this.store = store;

        $.GET('guild/list', function(res, data) {
            data.responseJSON.forEach(guild => {
                window.store.commit('updateGuild', guild);
            });
            cb();
        }.bind(this));
    }
    SelectGuild(guild, cb) {
        //Update guild css
        this.store.dispatch('selectGuild', {guildId: guild, cb: function(result) {
            this.UpdateGuildCss();
            cb();
        }.bind(this)});
    }
    UpdateGuildCss(guild) {

        const CSSParser = require('./CSSParser')
        const css = this.store.state.selectedGuild.guild.css;

        $("#guild-css")[0].innerHTML = CSSParser.parseCss(css, '.editable');
    }
}

module.exports = GuildManager;