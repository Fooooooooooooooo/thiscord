
class CustomParseObject {
    constructor(Name, RegexStr, cb) {
        this.Name = Name;
        this.Regex = new RegExp(RegexStr);
        this.cb = cb;
    }
}

class ParseObject {
    constructor(Name, RegexStr, StartChar, EndChar) {
        this.Name = Name;
        this.Regex = new RegExp(RegexStr);
        this.Start = StartChar;
        this.End = EndChar;
    }
}

class MsgParser
{
    parseMessage(message, messages)
    {

        // We need this to be in the order that we want as it will step through from start to end
        var parseObjects = [
            new ParseObject("Bold Italic", "(?:\\*\\*\\*)(.*?)(?:\\*\\*\\*)", "<b><i>", "</i></b>"),
            new ParseObject("Bold", "(?:\\*\\*)(.*?)(?:\\*\\*)", "<b>", "</b>"),
            new ParseObject("Italic", "(?:\\*)(.*?)(?:\\*)", "<i>", "</i>"),
            new ParseObject("Underline", "(?:__)(.*?)(?:__)", "<u>", "</u>"),
            new ParseObject("Italic2", "(?:_)(.*?)(?:_)", "<i>", "</i>"),
            new ParseObject("StrikeThrough", "(?:~~)(.*?)(?:~~)", "<strike>", "</strike>"),
            new ParseObject("Spoiler", "(?:\\|\\|)(.*?)(?:\\|\\|)", "<div class='spoiler'>", "</div>"),
            new ParseObject("Mentions", "(?:\<\@)([A-Za-z0-9]*?)(?:\>)", "<a class='mention' profile='", "'>@Unknown User</a>")
        ];

        var NewMessage = message.message;
        var TagCount = 0;

        parseObjects.forEach(function(Parso)
        {
            var RegexResult = NewMessage.split(Parso.Regex);
            if (RegexResult !== null && RegexResult.length > 1)
            {
                NewMessage = "";
                RegexResult.forEach(function(Str)
                {
                  // Counting while processing through this may be a bit much.
                  // Could probably be made into the for loop and itterate by 3s
                    TagCount += 1;
                    if (TagCount == 1 || TagCount == 3) {
                        NewMessage += Str;
                        if (TagCount >= 3) {
                          TagCount -= 3;
                        }
                    }
                    else if (TagCount == 2)
                    {
                        NewMessage += (Parso.Start + Str + Parso.End);
                    }
                });
            }
        });

        message.displayedMessage = "<div class=\"row\">" + NewMessage + "</div>";
        messages.push(message);
    }

    StackMessages(messages) {
        var previousAuthor = "";

        for(var i = 0; i < messages.length; i++) {

            var message = messages[i];

            if (message.type == 'Custom') {
                previousAuthor = message.sender_id;
                continue;
            }
  
            if (previousAuthor != message.sender_id || i < 1) {
                messages[i].displayed = true;
                previousAuthor = message.sender_id;
            } else {
  
                //Loop over messages backwards to find last shown message and append message to it
                const MAX_STACKED_MESSAGES = 5;
    
                for(var displayedMessage = i - 1; displayedMessage > (i - MAX_STACKED_MESSAGES) && displayedMessage >= 0; displayedMessage-- ) {
    
                    if (!messages[displayedMessage] || !messages[displayedMessage].displayed ) {
                        continue;
                    }
    
                    messages[i].displayed = false;
                    messages[displayedMessage].displayedMessage += "<div class=\"row\">" + message.displayedMessage + "</div>";
                    break;
                }

                if (messages[i].displayed != false) {
                    //Couldnt find message to stack on to
                    messages[i].displayed = true;
                }
            }
        }
    }

    CheckForCustomMessage(message, messages) {

        var customParsers = [
            new CustomParseObject('Invite Links', '(?:(?:https|http):\/\/thiscord.com\/i\/)([a-zA-Z]*)', function(data) {

                if (data.length <= 1) {
                    return;
                }
        
                messages.push({
                    type: 'Custom',
                    component: 'joinguild',
                    data: data[1]
                });
            }),
            new CustomParseObject('HTTP Links', /((?:[A-Za-z]{3,9}:(?:\/\/)?(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)(?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*)?)/, function(data, message) {
                var resultMessage = "";

                data.forEach(msg => {

                    if (!msg) {
                        return;
                    }

                    console.log(msg);
                    if (msg.match(/((?:[A-Za-z]{3,9}:(?:\/\/)?(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)(?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*)?)/)) {
                        resultMessage += "<a target=\"_blank\" href=\"" + msg + "\">" + msg + "</a>";
                    } else {
                        resultMessage += msg;
                    }
                });

                message.displayedMessage = resultMessage;
            })
        ]

        customParsers.forEach(parser => {
            var result = message.displayedMessage.split(parser.Regex);

            parser.cb(result, message);
        });
    }

    ParseMessages(messageList) {

        var messages = [];

        messageList.forEach(message => {
            message.type = 'Message';
            this.parseMessage(message, messages);
            this.CheckForCustomMessage(message, messages);
        });

        //Stack messages by the same author
        this.StackMessages(messages);

        return messages;
    }
}

module.exports = MsgParser;
