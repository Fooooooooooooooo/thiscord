@extends('layout.head')

@section("head")
    <title>ThisCord</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
@endsection

@section("content")
<body>
    @yield('content')
    <script src="{{ asset('js/global.js') }}"></script>
</body>
@endsection