const jwt = require('jsonwebtoken');

class Websockets {

    Setup(server) {

        //Setup variables
        this.channels = [];
        var channels = this.channels;

        this.users = [];
        var users = this.users;

        const socket = require('socket.io');
        var io = socket(server);

        io.on('connection', function(socket) {
            
            socket.on('auth', function(data) {
                try {
                    const decoded = jwt.verify(data.token, process.env.JWT_KEY);
            
                    const id = decoded.id;

                    //Create a count of users logged into account
                    if (users[id] == null) {
                        users[id] = [socket];
                    } else {
                        users[id].push(socket);
                    }

                    //Allow access to all routes
                    this.CreateRoutes(socket, channels, users, id);
                    
                } catch (error) {
                    console.log(error);
                    socket.disconnect();
                }
            }.bind(this));
        }.bind(this));
    }
    RemoveUserFromChannel(channels, channel, socket) {
        for (var i = 0; i < channels[channel].length; i++) {
            if (channels[channel][i] == socket) {
                channels[channel].splice(i);
                return;
            }
        }
    }
    CreateRoutes(socket, channels, users, id) {
        var listeningChannel = "";

        socket.on('chat', function(data) {
            if (!channels.includes(data.channel)) {
                //Channel dosen't exist
                return;
            }

            channels[data.channel].emit('chat-message', {
                message: data.message,
                user_id: "5d1f35ba0096f03199af0c1f"
            });
        });

        socket.on('join', function(data) {
            //If channel doesnt exist
            if (channels[data.channel] == null) {
                //TODO: Verify that channel exists first
                channels[data.channel] = [];
            }
            
            if (listeningChannel != "") {
                //They are switching channels
                this.RemoveUserFromChannel(channels, listeningChannel, socket);
            }

            channels[data.channel].push(socket);
            listeningChannel = data.channel;
        }.bind(this));

        socket.on('disconnect', function() {
            console.log('Got disconnect!');
      
            if (listeningChannel != "") {
                //They are switching channels
                this.RemoveUserFromChannel(channels, listeningChannel, socket);
            }

            //Create a count of users logged into account
            if (users[id].length == 1) {
                users[id] = null;
            } else {
                for (var i = 0; i < users[id].length; i++) {
                    if (users[id][i] == socket) {
                        users[id].splice(i);
                        return;
                    }
                }
            }
         }.bind(this));
    }
}

module.exports = new Websockets();