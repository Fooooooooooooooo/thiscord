//REST API
const http = require('http');
const app = require('./app');

const port = process.env.PORT || 8080;

const server = http.createServer(app);

server.listen(port);

//Websocket server
const sockets = require("./websockets");

sockets.Setup(server);
app.sockets = sockets;