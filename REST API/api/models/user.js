const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true }, 
    identifier: { type: String, default: Math.floor(1000 + Math.random() * 9000) },
    email: { type: String, required: true, unique: true, match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    presence: { type: Number, default: 0 },
    password: { type: String, required: true },
    created: { type: Date, required: true },
    guilds: [{ type : mongoose.Schema.Types.ObjectId, ref: 'Guild' }]
});

module.exports = mongoose.model('User', userSchema);