const mongoose = require('mongoose');

const guildSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    owner_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    name: { type: String, required: true }, 
    css: { type: String, default: "" },
    settings: { type: Object, required: true },
    created: { type: Date, required: true },
});

module.exports = mongoose.model('Guild', guildSchema);