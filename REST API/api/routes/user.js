const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const auth = require('../middleware/check-auth');

const User = require('../models/user');
const UserController = require('../controllers/UserController');

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || 
        file.mimetype === 'image/png' || 
        file.mimetype === 'image/gif') {
        cb (null, true);
    } else {
        cb (null, false);
    }
};

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/users/')
    },
    filename: function(req, file, cb) {
        cb(null, "tmp" + new Date().toISOString() + "." + file.originalname.split('.').pop())
    }
});

const upload = multer({storage: storage,
    limits: {
        //10mb
        fileSize: 1024 * 1024 * 10,
    },
    fileFilter: fileFilter
});

router.post('/register', upload.single('icon'), (req, res, next) => {

    if (req.body.password.length > 50 || req.body.password.length < 2) {
        return res.status(400).json({
            error: 'Invalid password'
        });
    }

    User.findOne({ email: req.body.email }).exec().then(user => {
        if (user) {
            return res.status(409).json({
                error: {
                    message: "Email address already in use"
                }
            });
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err.message
                    });
                } else {
                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        name: req.body.name,
                        email: req.body.email,
                        password: hash,
                        created: new Date()
                    });
        
                    user.save().then(result => {
                        if (req.file) {
                            //Move the file to be SERVER_ID.extension
                            fs.rename(req.file.path, req.file.destination + result._id + ".jpg");
                        }

                        res.status(201).json({
                            message: "Success"
                        });

                    }).catch(err => {
                        res.status(500).json({
                            error: {
                                message: err.message
                            }
                        })
                    });
                }
            });
        }
    }).catch(err => {
        return res.status(500).json({
            error: err.message
        });
    });
});

router.post('/login', (req, res, next) => {
    User.findOne({ email: req.body.email }).exec().then(user => {
        if (!user) {
            return res.status(400).json({
                error: 'Auth failed'
            });
        }

        bcrypt.compare(req.body.password, user.password, (err, same) => {
            if (err) {
                return res.status(400).json({
                    error: 'Auth failed'
                });
            }

            if (same) {
                //Create signed token
                const token = jwt.sign({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    identifier: user.identifier,
                }, process.env.JWT_KEY, {
                    expiresIn: "1 day"
                });

                return res.status(200).json({
                    message: 'Auth successful',
                    token: token
                });
            } else {
                return res.status(400).json({
                    error: 'Auth failed'
                });
            }
        });

    }).catch(err => {
        return res.status(400).json({
            error: err.message
        });
    });
});

router.delete('/:userId/', auth, (req, res, next) => {

    if (req.params.userId !== req.user.id) {
        return res.status(400).json({ error: 'Auth failed' });
    }

    User.findByIdAndDelete(req.params.userId).exec().then(result => {
        res.status(200).json({message: "Success"});
    }).catch(err => {
        return res.status(400).json({
            error: err.message
        });
    });
});

router.get('/current', auth, (req, res, next) => {
    User.findById(req.user.id).exec().then(result => {
        if (result) {
            res.status(200).json({
                _id: result.id,
                name: result.name,
                identifier: result.identifier,
                email: result.email,
                created: result.created
            });
        } else {
            res.status(404).json({message: "User not found"});
        }
    }).catch(err => {
        return res.status(500).json({
            error: err.message
        });
    });
});

router.get('/:userId', auth, (req, res, next) => {
    User.findById(req.params.userId).exec().then(result => {
        if (result) {
            res.status(200).json({
                _id: result.id,
                name: result.name,
                identifier: result.identifier,
                created: result.created
            });
        } else {
            res.status(404).json({message: "User not found"});
        }
    }).catch(err => {
        return res.status(500).json({
            error: err.message
        });
    });
});

router.post('/:userId/join', auth,  (req, res, next) => UserController.JoinGuild(req, res));

module.exports = router;