const express = require('express');
const router = express.Router();
const Guild = require('../models/guild');
const User = require('../models/user');
const Channel = require('../models/channel');
const mongoose = require('mongoose');
const GuildController = require('../controllers/GuildController');
const UserController = require('../controllers/UserController');
const multer = require('multer');

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || 
        file.mimetype === 'image/png' || 
        file.mimetype === 'image/gif') {
        cb (null, true);
    } else {
        cb (null, false);
    }
};

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/guilds/')
    },
    filename: function(req, file, cb) {
        cb(null, "tmp" + new Date().toISOString() + "." + file.originalname.split('.').pop())
    }
});

const upload = multer({storage: storage,
    limits: {
        //10mb
        fileSize: 1024 * 1024 * 10,
    },
    fileFilter: fileFilter
});

router.get('/list', (req, res, next) => UserController.ListGuilds(req, res));

router.post('/create', upload.single('icon'), (req, res, next) => GuildController.Create(req, res));

router.get('/:guildId', (req, res, next) => {
    const id = req.params.guildId;

    Guild.findById(id).exec().then(guild => {

        if (guild) {
            //Get channels
            Channel.find({ guild: id }).exec().then(channel => {

                const response = {
                    _id: guild.id,
                    name: guild.name,
                    settings: guild.settings,
                    owner_id: guild.owner_id,
                    created: guild.created,
                    css: guild.css,
                    channels: {}
                };

                channel.forEach((element, index) => {
                    response.channels[element._id] = {
                        _id: element._id,
                        name: element.name,
                        created: element.created,
                        topic: element.topic,
                        css: element.css
                    };
                });

                res.status(200).json(response);
            }).catch(err => {
                console.log(err);
                res.status(500).json({ error: { message: "Error fetching channels" }});
            });
            
        } else {
            res.status(404).json({ error: { message: "No guild found" }});
        }
        
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: { message: "Invalid guild ID" }});
    });
});

router.get('/:guildId/users', (req, res, next) => {
    const id = req.params.guildId;

    User.find({ guilds: id }).exec().then(users => {

        var userMap = {};

        users.forEach(function(user) {
            userMap[user._id] = {
                _id: user._id,
                name: user.name,
                identifier: user.identifier,
                presence: req.app.sockets.users[user._id] ? 2 : 0
            };
        });

        res.status(200).json(userMap);  
        
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: { message: err.message }});
    });
});

router.patch('/:guildId', (req, res, next) => {

    const changes = {};

    //Only allow name, css and settings to be changed
    if (req.body.name) {
        changes.name = req.body.name;
    }

    if (req.body.css) {
        changes.css = req.body.css;
    }

    if (req.body.settings) {
        changes.settings = req.body.settings;
    }

    console.log(req.body.name);
    
    Guild.findOneAndUpdate({ _id: req.params.guildId }, changes).exec().then(result => {
        console.log(result);
        if (result) {
            res.status(200).json({ message: "Success" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => {
        console.log(err);
        res.status(500).json({error: { message: err.message }});
    });

});

router.delete('/:guildId', (req, res, next) => {
    const id = req.params.guildId;
    Guild.findOneAndRemove({ _id: req.params.messageId, owner_id: req.user.id }).exec().then(result => {
        if (result) {
            res.status(200).json({ message: "Successfully deleted object" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => {
        console.log(err);
        res.status(500);
    });
});

module.exports = router;