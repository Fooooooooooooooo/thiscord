const User = require('../models/user');
const mongoose = require('mongoose');

exports.JoinGuild = function(req, res) {
    if (req.user.id != req.params.userId) {
        return res.status(401).json({ error: 'Auth failed' });
    }

    User.findById(req.user.id).populate('guilds').then(result => {
        if (result.guilds.includes(mongoose.Types.ObjectId(req.body.guildId))) {
            res.status(400).json({ error: { message: "User is already in guild" }});
            return;
        }
        result.guilds.push(req.body.guildId);
        result.save();
        res.status(200).json({ message: "Success"});
    }).catch(err => {
        console.log("Error! " + err);;
        res.status(404).json({ error: { message: err.message }});
    });
}

exports.ListGuilds = function(req, res) {

    User.findById(req.user.id).populate('guilds').then(result => {

        var guilds = [];

        result.guilds.forEach(guild => {
            guilds.push({
                _id: guild._id,
                name: guild.name,
                settings: guild.settings,
                owner_id: guild.owner_id,
                created: guild.created,
                css: guild.css
            });
        });

        res.status(200).json(guilds);
    }).catch(err => {
        console.log("Error! " + err);;
        res.status(404).json({ error: { message: err.message }});
    });

}