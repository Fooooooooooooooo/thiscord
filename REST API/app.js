const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const auth = require('./api/middleware/check-auth');

const uploadRoutes = require('./api/routes/uploads');
const guildRoutes = require('./api/routes/guild');
const channelRoutes = require('./api/routes/channel');
const userRoutes = require('./api/routes/user');
const messageRoutes = require('./api/routes/message');

mongoose.connect('mongodb://localhost/database', { useNewUrlParser: true });

//Log all requests
app.use(uploadRoutes);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use((req, res, next) => {

    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Headers", "Authorization, x-socket-id, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    if (req.method === 'OPTIONS') {
        res.setHeader("Access-Control-Allow-Methods", "GET, PATCH, HEAD, OPTIONS, POST, PUT, DELETE");
        return res.status(200).json({});
    }

    next();
});

app.use(morgan('dev'))

//Listen for routes
app.get('/', (req, res, next) => {
    res.status(301).redirect('https://gitlab.com/DarkZek/thiscord/blob/master/API%20Specification.md');   
});
app.use('/guild', auth, guildRoutes);
app.use('/guild', auth, messageRoutes);
app.use('/guild', auth, channelRoutes);
app.use('/user/', userRoutes);

app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
        error: {
            message: error.message
        }
    });
    console.log("There was an error: " + error);
});

module.exports = app;